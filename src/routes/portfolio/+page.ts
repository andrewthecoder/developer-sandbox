import type { Repo } from '$lib/types'

export async function load({ fetch }: { fetch: WindowOrWorkerGlobalScope['fetch'] }) {
  const response = await fetch('https://api.github.com/users/erwininteractive/repos?per_page=50')
  const unfiltered: Repo[] = await response.json()

  const repos = unfiltered.filter((repo) => repo.topics.includes('portfolio'))

  return { repos }
}
