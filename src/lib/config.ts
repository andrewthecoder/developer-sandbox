import { dev } from '$app/environment'

export const title = 'the developer sandbox'
export const author = 'andrew s erwin'
export const description = 'articles and tutorials to help keep software engineers current'
export const url = dev ? 'http://localhost:5173' : 'https://developersandbox.xyz'
