export type Categories = string

export type Topic = string

export type Languages = string

export type Post = {
  title: string
  slug: string
  description: string
  date: string
  categories: Categories[]
  published: boolean
}

export type Repo = {
  id: number
  name: string
  html_url: string
  description: string
  language: string
  has_wiki: boolean
  homepage: string | null
  topics: Topic[]
  languages_url: string
}
