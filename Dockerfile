FROM node AS build

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN pnpm install
COPY . ./
RUN pnpm run build

FROM nginx:alpine
COPY --from-build /app/public /usr/share/nginx/html

